<?php
class Controller{

var $mapper=array(
"info"=>array(
"manufacturer"=>0
),
"rated"=>array(
"pv_rated_voltage"=>0,
"pv_rated_current"=>1,
"pv_rated_power"=>2,
"battery_rated_voltage"=>3,
"rated_charging_current"=>4,
"rated_charging_power"=>5,
"charging_mode"=>6,
"rated_load_current"=>7
),
"realtime"=>array(
"pv_voltage"=>0,
"pv_current"=>1,
"pv_power"=>2,
"battery_voltage"=>3,
"battery_charging_current"=>4,
"battery_charging_power"=>5,
"load_voltage"=>6,
"load_current"=>7,
"load_power"=>8,
"battery_temperature"=>9,
"charger_temperature"=>10,
"heatsink_temperature"=>11,
"remote_battery_temperature"=>13,
),
"stat"=>array(
"max_pv_voltage_today"=>0,
"min_pv_voltage_today"=>1,
"max_battery_voltage_today"=>2,
"min_battery_voltage_today"=>3,
"consumed_energy_today"=>4,
"consumed_energy_month"=>5,
"consumed_energy_year"=>6,
"consumed_energy_total"=>7,
"generated_energy_today"=>8,
"generated_energy_month"=>9,
"generated_energy_year"=>10,
"generated_energy_total"=>11,
"ambient_temperature"=>15,
),
"setting"=>array(
"second"=>15,
"minute"=>16,
"hour"=>17,
"day"=>18,
"month"=>19,
"year"=>20,
)


);

function __construct($tracer){

if ($tracer->getInfoData()) {
/*
	print "Info Data\n";
	print "----------------------------------\n";
	for ($i = 0; $i < count($tracer->infoData); $i++)
		print $i."-".str_pad($i, 2, '0', STR_PAD_LEFT)." ".$tracer->infoKey[$i].": ".$tracer->infoData[$i]."\n";
	} else print "Cannot get Info Data\n";
*/
	foreach ($this->mapper['info'] as $k=>$v){
		$this->$k=$tracer->infoData[$v];
}
}

if ($tracer->getRatedData()) {
/*	print "Rated Data\n";
	print "----------------------------------\n";
	for ($i = 0; $i < count($tracer->ratedData); $i++)
		print $i."-".str_pad($i, 2, '0', STR_PAD_LEFT)." ".$tracer->ratedKey[$i].": ".$tracer->ratedData[$i].$tracer->ratedSym[$i]."\n";
	} else print "Cannot get Rated Data\n";
*/
        foreach ($this->mapper['rated'] as $k=>$v){
                $this->$k=$tracer->ratedData[$v];
        }

}
if ($tracer->getRealtimeData()) {
/*	print "\nRealTime Data\n";
	print "----------------------------------\n";
	for ($i = 0; $i < count($tracer->realtimeData); $i++)
		print $i."-".str_pad($i, 2, '0', STR_PAD_LEFT)." ".$tracer->realtimeKey[$i].": ".$tracer->realtimeData[$i].$tracer->realtimeSym[$i]."\n";
	} else print "Cannot get RealTime Data\n";
*/
        foreach ($this->mapper['realtime'] as $k=>$v){
                $this->$k=$tracer->realtimeData[$v];
        }
}
if ($tracer->getStatData()) {
/*	print "\nStatistical Data\n";
	print "----------------------------------\n";
	for ($i = 0; $i < count($tracer->statData); $i++)
		print $i."-".str_pad($i, 2, '0', STR_PAD_LEFT)." ".$tracer->statKey[$i].": ".$tracer->statData[$i].$tracer->statSym[$i]."\n";
	} else print "Cannot get Statistical Data\n";
*/
        foreach ($this->mapper['stat'] as $k=>$v){
                $this->$k=$tracer->statData[$v];
        }
}
	
if ($tracer->getSettingData()) {
/*	print "\nSettings Data\n";
	print "----------------------------------\n";
	for ($i = 0; $i < count($tracer->settingData); $i++)
		print $i."-".str_pad($i, 2, '0', STR_PAD_LEFT)." ".$tracer->settingKey[$i].": ".$tracer->settingData[$i].$tracer->settingSym[$i]."\n";
	} else print "Cannot get Settings Data\n";
*/
        foreach ($this->mapper['setting'] as $k=>$v){
                $this->$k=$tracer->settingData[$v];
        }

$this->current_date=(2000+$this->year).'-'.$this->month.'-'.$this->day;
$this->current_time=$this->hour.':'.$this->minute.':'.$this->second;
$this->battery_current=$this->battery_charging_current-$this->load_current;
//glitch! charging at night 
if($this->pv_current==0){
$this->battery_charging_current=0;
}
}
/*
if ($tracer->getCoilData()) {
	print "\nCoils Data\n";
	print "----------------------------------\n";
	for ($i = 0; $i < count($tracer->coilData); $i++)
		print $i."-".str_pad($i, 2, '0', STR_PAD_LEFT)." ".$tracer->coilKey[$i].": ".$tracer->coilData[$i]."\n";
	} else print "Cannot get Coil Data\n";

if ($tracer->getDiscreteData()) {
	print "\nDiscrete Data\n";
	print "----------------------------------\n";
	for ($i = 0; $i < count($tracer->discreteData); $i++)
		print $i."-".str_pad($i, 2, '0', STR_PAD_LEFT)." ".$tracer->discreteKey[$i].": ".$tracer->discreteData[$i]."\n";
	} else print "Cannot get Discrete Data\n";

}
*/

}
}
