#!/usr/bin/env php
<?php

require_once 'inc/PhpEpsolarTracer.php';
include "inc/controller.php";
include "config.php";
$tracer = new PhpEpsolarTracer($device);

$c=new Controller($tracer);

$params=array('secret'=>$secret, 'data'=>json_encode($c));
$defaults = array(
CURLOPT_URL => $target, 
CURLOPT_POST => true,
CURLOPT_POSTFIELDS => $params,
);
$ch = curl_init();
curl_setopt_array($ch, ($defaults));
curl_exec($ch);
curl_close($ch);

//var_dump($ch);
